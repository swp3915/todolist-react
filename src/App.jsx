import { useState } from "react";
import "./App.css";
import TodoList from "./component/TodoList";
import AddTodo from "./component/AddTodo";

function App() {
  const [todo, setTodo] = useState([]);
  return (
    <>
      <AddTodo todo={todo} setTodo={setTodo} />
      <TodoList todo={todo} setTodo={setTodo} />
    </>
  );
}

export default App;
