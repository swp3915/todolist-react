function DeleteTodo({ index, todo, setTodo }) {
  const handleDelete = () => {
    setTodo((todo) => todo.filter((prevTodo) => prevTodo.id !== todo[index].id));
  };
  return (
    <>
      <button onClick={handleDelete}>Delete</button>
    </>
  );
}

export default DeleteTodo;
