import { useState } from "react";

let id = 0;
function AddTodo({ todo, setTodo }) {
  const [input, setInput] = useState("");

  const changeTextInput = (e) => {
    setInput(e.target.value);
  };

  const handleAdd = () => {
    id++;
    setTodo((todo) => [
      ...todo,
      {
        id: id,
        name: input,
        isCompleted: false,
        isEdited: false,
      },
    ]);
  };

  return (
    <>
      <input onChange={changeTextInput} />
      <button onClick={handleAdd}>+</button>
    </>
  );
}

export default AddTodo;
