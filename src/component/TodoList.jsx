import TodoItem from "./TodoItem";

function TodoList({ todo, setTodo }) {
  return (
    <>
      {/* <TodoItem todo={todo}/> */}
      {todo.map((t, i) => {
        return (
          <TodoItem key={t.id} index={i} todo={todo} setTodo={setTodo}/>
        );
      })}
    </>
  );
}

export default TodoList;
