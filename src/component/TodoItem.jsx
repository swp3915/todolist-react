import DeleteTodo from "./DeleteTodo";
import "../index.css";

function TodoItem({ index, todo, setTodo }) {
  const handleCheckbox = () =>
    setTodo(
      todo.map((item) =>
        item.id === todo[index].id
          ? { ...item, isCompleted: !item.isCompleted }
          : item
      )
    );

  const handleChangeEdit = (e) => {
    setTodo(
      todo.map((item) =>
        item.id === todo[index].id
          ? { ...item, isEdited: !item.isEdited }
          : item
      )
    );
  };

  const handleEdit = (e) => {
    if (e.key === "Enter") {
      setTodo(
        todo.map((item) =>
          item.id === todo[index].id
            ? { ...item, isEdited: !item.isEdited, name: e.target.value }
            : item
        )
      );
    }
  };
  return (
    <>
      <br></br>
      {todo[index].isEdited ? (
        <input defaultValue={todo[index].name} onKeyDown={handleEdit} />
      ) : (
        <>
          <input
            type="checkbox"
            checked={todo[index].isCompleted}
            onChange={handleCheckbox}
          />
          <label
            className={todo[index].isCompleted ? "done" : ""}
            onDoubleClick={handleChangeEdit}
          >
            {todo[index].name}
          </label>
          <DeleteTodo index={index} todo={todo} setTodo={setTodo}></DeleteTodo>
        </>
      )}
    </>
  );
}

export default TodoItem;
